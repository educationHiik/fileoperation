/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileoperation;

/**
 *
 * @author vaganovdv
 */
public class FileOperation
{

    public static void main(String[] args)
    {
        WriteFile fileWriter = new WriteFile();
        fileWriter.write("Учебное занятие от 06.06.2020"); 
        
        ReadFile readFile = new ReadFile();
        String messageFromFile = readFile.read();
        System.out.println("Получено из файла :" +messageFromFile);
        
        if (messageFromFile != null && !messageFromFile.isEmpty())
        {
            TcpClient client = new TcpClient("localhost", 7777);
            if (client.startClient())
            {
                client.writeFileToServer(readFile.getFileBuffer());
            }
        }   
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileoperation;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vaganovdv
 */
public class TcpClient
{
    private Socket clientSocket;
    private String   host  = "localhost";
    private int      port  = 7777;
    
    private InputStream is;
    private OutputStream os;
    

     public TcpClient(String host, int port)
    {
        this.host = host;
        this.port = port;
    }
   
     
     
    public boolean startClient()
    {
        boolean isStart = false;
        try
        {
            System.out.println("Подключение к удаленному TCP серверу ");
            System.out.println("IP   = {" + getHost() + "}");
            System.out.println("port = {" + getPort() + "}");
            clientSocket = new Socket();
            InetAddress addr = InetAddress.getByName(getHost());
            SocketAddress socketAddress = new InetSocketAddress(addr, getPort());

            clientSocket.connect(socketAddress);
            System.out.println("Подключено к серверу");
           
            // Формирование потоков 
            is = clientSocket.getInputStream();
            os = clientSocket.getOutputStream();
            
            if (is != null && os != null)
            {
                isStart = true;
                System.out.println("Клиент стартовал успено");
            }   
        } catch (IOException e)
        {
            System.out.println("Ошибка сетевого соединения: {" + e.getMessage() + "}");
        } 
        return isStart;
    }
    
    
    
    
    public void closeClinet()
    {
        System.out.println("Заверешение работы клиента");
        try
        {
            if (is != null)
            {
                is.close();
            }
            if (os != null)
            {
                os.close();
            }
            if (clientSocket != null && !clientSocket.isClosed())
            {
                clientSocket.close();
            }
        } catch (IOException e)
        {
            System.out.println("Ошибка сетевого соединения при завершении работы: {" + e.getMessage() + "}");
        }
    }


    
    public void writeFileToServer(byte[] fileBuffer)
    {
        if (fileBuffer != null && fileBuffer.length > 0)
        {
            if (clientSocket.isConnected())
            {
                try
                {
                    OutputStream os = clientSocket.getOutputStream();
                    DataOutputStream dos = new DataOutputStream(os);
                    // Определение размера буфера для передачи 
                    int bytes = fileBuffer.length;

                    // передача байтов файла через сетевое соединение 
                    dos.writeInt(bytes);
                    dos.write(fileBuffer, 0, bytes);
                    dos.flush();
                    dos.close();
                    System.out.println("Байты переданы через сетевое соедиенение");
                    clientSocket.close();
                } catch (IOException ex)
                {
                    Logger.getLogger(TcpClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else
            {
                System.out.println("Ошибка отправки буфера через сетевое соединение: отсутвует подключение к серверу");
            }
        }
        else
        {
            System.out.println("Ошибка отправки буфера через сетевое соединение: файловый буфер пустой ");
        }

    }


    
    
      /**
     * @return the host
     */
    public String getHost()
    {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host)
    {
        this.host = host;
    }

    /**
     * @return the port
     */
    public int getPort()
    {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port)
    {
        this.port = port;
    }
    

}

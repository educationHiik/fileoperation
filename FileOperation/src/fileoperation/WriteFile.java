/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileoperation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author vaganovdv
 */
public class WriteFile
{
    
    private   String filePath = "/home/vaganovdv/git/gitlab/fileoperation";
    private   String textFile = "test.txt";
    private   String fullPath  = filePath + File.separator + textFile ;

    private Socket socket;
    
    public WriteFile()
    {
         System.out.println("Полный путь к файлу: "+fullPath);
    }
    
    
    public void write(String message)
    {
        File file = new File(fullPath);
        try
        {
            //     
            // true false
            boolean result = file.createNewFile();
            if (result)
            {
                System.out.println("Файл создан успешно");
                FileOutputStream fos = new FileOutputStream(file);

                // Создание массива out с вложенным сообщением из строки message
                byte[] out = message.getBytes();

                System.out.println("Попытка записи: {" + out.length + "} байт");

                fos.write(out);  // Запись в поток
                fos.flush();     // Физическое размещение байтов потока на диске   
                fos.close();
                System.out.println("Запись в файл заверешена");
            }

        } catch (IOException ex)
        {
            System.out.println("Ошибка: файл не создан");
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileoperation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author vaganovdv
 */
public class ReadFile
{

    
    private   String filePath = "/home/vaganovdv/git/gitlab/fileoperation";
    private   String textFile = "test.txt";
    private   String fullPath  = filePath + File.separator + textFile ;
    private   byte [] fileBuffer; 
    
    public ReadFile()
    {
        
        
    }
    
    public String read()
    {
        File file = new File(fullPath);
        String fromFile = "";

        // Проверка существования файла
        if (file.exists() && !file.isDirectory())
        {
            try
            {
                // Создание входного потока к файлу
                FileInputStream fis = new FileInputStream(file);
                // Исследование потока - вяснение размера
                int bytes = fis.available();
                System.out.println("Достпуно в потоке: {" + bytes + "} байт");
                // Выделение памяти под файл в оперативной памяти 
                fileBuffer = new byte[bytes];

                // Чтение содержимого диска в массив в памяти
                fis.read(fileBuffer, 0, bytes);
                System.out.println("Прочитано и размещено в памяти: " + fileBuffer.length);

                // Преобразование байтов в строку 
                fromFile = new String(fileBuffer);

            } catch (FileNotFoundException ex)
            {
                System.out.println("Файл не существует : {" + fullPath + "}");
            } catch (IOException ex)
            {

            }
        } else
        {
            System.out.println("Файл не существует : {" + fullPath + "}");
        }
        return fromFile;
    }

    
    /**
     * @return the fileBuffer
     */
    public byte[] getFileBuffer()
    {
        return fileBuffer;
    }

  
    
    
}

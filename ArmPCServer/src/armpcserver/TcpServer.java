/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package armpcserver;


import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author vaganovdv
 */
public class TcpServer
{

    private  Socket clientSocket;
    private  ServerSocket server;
    private  String   host  = "localhost";
    private  int      port  = 7777;

    public TcpServer(String host, int port)
    {
        this.host = host;
        this.port = port;
    }
    
    
    public void startServer()
    {
    
        try
        {
            // Запуск сервера
            server = new ServerSocket(port); // Создание сервера для обработки запросов 
            System.out.println("Сервер стартовал на порту {"+server.getLocalPort()+"}");
            
            // Запуск клиента 
            System.out.println("Ожидание подключения клиента...");
            clientSocket = server.accept(); // Обработка подключения клиента 
            System.out.println("Подключен клиент");
            InputStream is = clientSocket.getInputStream();
            DataInputStream dis = new DataInputStream(is);

            
            
            
            boolean stopReading = true; // Признак окончания потока
            int fileLength = dis.readInt();
            int byteCount = -1;
            System.out.println("Количество байт от клиента: {" + fileLength + "}");

            byte[] stringBuf = new byte[fileLength];

            while (stopReading)
            {
                byte b = dis.readByte();
                byteCount++;
                System.out.println("[" + byteCount + "] байт : {" + toHexString(b) + "}");
                stringBuf[byteCount] = b;

                
                // Проверка получения строки 
                if (byteCount >= fileLength - 1)
                {
                    String out = new String(stringBuf);
                    System.out.println("Получена строка: " + out);
                    break;
                }
            }
        } catch (IOException ex)
        {
            System.out.println("Ошибка старта сервера: " + ex);
            
        } 
        
        // Вызов функции остановки сервера
        stopServer();
    }        
    
    
    
    public void stopServer()
    {
        System.out.println("Завершение работы программы...");
        if (server != null)
        {   
            if (clientSocket != null)    
            {
                if (!clientSocket.isClosed())
                {
                    System.out.println("Закрытие соедиенения с клиентом ...");
                    try
                    {
                        clientSocket.close();
                        System.out.println("Соединение с клиентом закрыто");
                        if (!server.isClosed())
                        {
                            System.out.println("Остановка сервера... ");
                            server.close();
                            System.out.println("Сервер остановлен");
                        }        
                    } catch (IOException ex)
                    {
                        System.out.println("Ошибка операции ввода-вывода при остановке сервера...");
                        System.out.println("Описание ошибки {"+ex.toString()+"}");
                    }
                }       
            }    
        }   
    }        
    
      
    public String toHexString(Byte b)
    {
        byte[] byteBuf = new byte[1];
        byteBuf[0] = b;
        String byteString = Hex.encodeHexString(byteBuf);
        return byteString;
    }
    
    
    
    
    public Socket getClientSocket()
    {
        return clientSocket;
    }

       public void setClientSocket(Socket clientSocket)
    {
        this.clientSocket = clientSocket;
    }

   
    public String getHost()
    {
        return host;
    }

   
    public void setHost(String host)
    {
        this.host = host;
    }

    
    public int getPort()
    {
        return port;
    }
    
    public void setPort(int port)
    {
        this.port = port;
    }
   
}
